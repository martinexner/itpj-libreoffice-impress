---
geometry: margin=2cm
---


# Diagramme und Videos

## Diagramme

Eine Möglichkeit, Daten noch übersichtlicher darzustellen als in Tabellen, sind Diagramme. Und die wollen wir uns auch gleich anschauen:

Wir erstellen wieder eine neue Folie, vergeben einen passenden Titel und klicken dann in der oberen Symbolleiste auf den Diagramm-Button (oder gehen alternativ über die Menüleiste auf **Einfügen** und auf **Diagramm**):

![](img/2_11_chart_1.png)

Wenn wir diesen Button klicken, erscheint auf unserer Folie auch gleich ein Diagramm im Bearbeitungsmodus.
Jetzt wollen wir zuerst einmal die Daten eingeben, die unser Diagramm darstellen soll.
Wir klicken dazu mit der rechten Maustaste auf unser Diagramm und wählen **Datentabelle bearbeiten**.
Wir bekommen dadurch dieses Fenster:

![](img/2_11_chart_2.png)

Standardmäßig bekommen wir hier eine Datentabelle mit 4 Zeilen und 4 Spalten. Wir brauchen aber nur 3 Zeilen und 2 Spalten, und müssen die restlichen somit löschen.
Das machen wir, indem wir zuerst auf irgendeine Zelle in der letzten Zeile klicken, und dann auf den **Zeile löschen**-Button klicken. Dieser hat ein Symbol einer 3x3-Tabelle, in der eine Zeile rot markiert ist.

Jetzt müssen wir noch die zwei überflüssigen Spalten löschen. Dazu markieren wir irgendeine Zelle in der letzten Spalte und klicken auf den **Datenreihe löschen**-Button:

![](img/2_11_chart_3.png)

Das machen wir gleich nocheinmal, um auch die zweite überflüssige Spalte zu löschen.

Jetzt sollten wir eine Datentabelle haben, die aus 2 Spalten und 3 Zeilen besteht.

Fehlen nur noch die Daten selber:

- In der linken Spalte mit dem Namen **Kategorien** tippen wir in die Zellen untereinander "**LibreOffice Impress**", "**Google Slides**" und "**Microsoft PowerPoint**"
- In der rechten Spalte mit dem Namen **Y-Werte**" tippen wir "**100**", "**60**" und "**30**"
- Über der rechten Spalte (**Y-Werte**") gibt es noch die Möglichkeit, der Datenreihe einen Namen zu geben. Hier tippen wir "**Coolness**" ein.

Die Datentabelle sollte jetzt so aussehen:

![](img/2_11_chart_4.png)

Man erahnt jetzt vielleicht schon, welche Daten wir unseren Zusehern mit diesem Diagramm präsentieren wollen: Nämlich dass LibreOffice Impress das mit Abstand coolste Präsentationsprogramm ist!

Wenn wir das Datentabelle-Fenster mit **OK** schließen, sehen wir wieder unser Diagramm, das aktuell so aussehen sollte:

![](img/2_11_chart_5.png)

Wenn wir jetzt auf einen der Balken klicken, können wir rechts in den Eigenschaften noch einiges Einstellen, zum Beispiel die Farbe (unter dem Punkt **Fläche**). Hier wählen wir ein kräftiges Dunkelrot.

Unsere fertige Folie mit dem Coolness-Diagramm sollte dann so aussehen:

![](img/2_11_chart_6.png)

## Videos

Auch Videos lassen sich in Präsentationen einbinden.
Dazu brauchen wir aber zuerst ein Video.
Das holen wir uns, indem wir wieder den Webbrowser starten und auf `peach.blender.org` gehen:

![](img/2_12_video_1.png)

Dort klicken wir auf **Download**:

![](img/2_12_video_2.png)

Und dann auf den ersten Link unter dem Hinweis "NOTE: This Page Is From 2008, Links Might Have Expired. Instead, Get The Old Film Release Here:".
Dann landen wir hier:

![](img/2_12_video_3.png)

Um Bandbreite zu sparen, nehmen wir hier die Version mit der geringsten Auflösung, nämlich `BigBuckBunny_320x180.mp4`.
Wir speichern dieses Video, indem wir darauf klicken und sich ein Download-Popup öffnet, das wir akzeptieren. Falls das Video direkt im Browser zu spielen beginnt, gehen wir nochmal zurück zu der Seite im letzten Screenshot oben, klicken statt der linken mit der rechten Maustaste auf die Videodatei und wählen **Speichern unter**. Wo wir die Datei hinspeichern ist egal, aber wir müssen sie nachher wieder finden.

Dann erstellen wir in LibreOffice Impress wieder eine neue Folie, vergeben einen passenden Titel und klicken dann oben in der oberen Symbolleiste auf den Audio-oder-Video-Button (oder gehen alternativ über die Menüleiste auf **Einfügen** und **Audio oder Video**).
Dadurch öffnet sich ein Fenster, in dem wir die vorher heruntergeladene und gespeicherte Videodatei öffnen:

![](img/2_12_video_4.png)

Wenn wir die Datei geöffnet haben, wird das Video als Standbild in unsere Folie eingefügt. Womöglich müssen wir noch die Größe und Position auf der Folie anpassen, danach sollte die Folie in etwa so aussehen:

![](img/2_12_video_5.png)

Wenn wir die Präsentation jetzt starten und zu unserer Video-Folie springen, sollte das Video zu spielen beginnen:

![](img/2_12_video_6.png)
