
# Übungsblatt 4: Diagramme

1. Erstelle eine neue Präsentation, die aus einer Folie besteht.
2. Füge ein Tortendiagramm ein, in dem die einzelnen Teile deinen Hobbies entsprechen. Wenn du zum Beispiel in deiner Freizeit sehr viel kochst und gelegentlich fernsiehst und Bücher liest, könnte das Tortendiagramm so aussehen:

![](img/5_ue4_chart_1.png)
