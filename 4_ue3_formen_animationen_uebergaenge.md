
# Übungsblatt 3: Formen, Animationen, Übergänge

1. Erstelle eine neue Präsentation, die aus einer Folie besteht.
2. Füge einen **dunkelgrünen** Stern mit **12 Zacken ein**, der eine **graue** Umrandung mit **2pt** Linienbreite hat.
3. Animiere den Stern mit dem **Drehen**-Effekt.
