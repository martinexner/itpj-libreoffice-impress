---
geometry: margin=2cm
---


# Notizen, Handouts und Export

## Notizen

Ein weiteres praktisches Feature von LibreOffice Impress sind Notizen.
Notizen kann man sich pro Folie machen, und diese kann man sich als Vortragender dann während der Präsentation anzeigen lassen, so dass man selbst die aktuelle Folie und die Notizen sieht, während die Zuschauer nur die Folie sehen. Dazu braucht man allerdings einen zweiten Bildschirm (was aber meistens der Fall ist wenn man mit Beamer präsentiert, weil der Beamer oft als eigener Bildschirm zählt).

Um die Notizen einer Folie bearbeiten zu können, klicken wir oben in der Menüleiste auf **Ansicht** und dann auf **Notizen**.

Die Ansicht ändert sich jetzt im Vergleich zu vorhin etwas, nämlich sehen wir jetzt jeweils die Folie und darunter die Notizen, können aber nur die Notizen bearbeiten und nicht die Folie selbst:

![](img/5_1_notes_1.png)

Hier empfiehlt es sich, sich nur Stichworte zu notieren, damit man während der Präsentation nicht verleitet ist, die ganze Zeit nur abzulesen.
Zum Beispiel so:

![](img/5_1_notes_2.png)

Wenn wir jetzt die Präsentation starten, dann sehen wir am Bildschirm des Vortragenden standardmäßig folgendes:

![](img/5_1_notes_3.png)

Auf dem Bildschirm (bzw. Beamer) der Zuschauer sehen wir ganz normal die Folie im Vollbildmodus:

![](img/5_1_notes_4.png)

(Wie gesagt, hierfür brauchen wir zwei Monitore bzw. einen Monitor und einen Beamer.)

Wenn wir am Vortragenden-Monitor in der unteren Leiste jetzt auf **Anmerkung** klicken, bekommen wir während der Präsentation zusätzlich auch noch unsere Notizen angezeigt:

![](img/5_1_notes_5.png)


## Präsentation exportieren

Zum Schluss wollen wir uns noch ansehen wie wir von unserer Präsentation Handouts erstellen können und wie wir unsere Präsentation in anderen Dateiformaten, z.B. für Microsoft PowerPoint, exportieren können:

### Handouts

Handouts sind Übersichten über die Folien einer Präsentation, entweder ausgedruckt auf Papier oder im PDF-Format. Dabei landet üblicherweise nicht jede Folie auf ihrer eigenen Seite, sondern es werden immer mehrere Folien gemeinsam auf einer Seite gruppiert.

Natürlich können wir auch aus LibreOffice Impress Handouts erstellen!
Dazu klicken wir ganz oben in der Menüleiste zuerst auf **Datei** und dann auf **Drucken**.
Nehmen wir einmal an, wir wollen die Handouts in PDF-Form exportieren. Dazu wählen wir jetzt als Drucker **In Datei drucken...** aus.

Wir sollten jetzt ein Fenster sehen, das in etwa so aussieht:

![](img/6_1_handouts_1.png)

Aktuell würden wir jede Folie noch auf ihrer eigenen Seite bekommen, was wir aber nicht wollen.
Also wählen wir weiter unten, im Abschnitt **Ausdruck**, für **Dokument** statt **Folien** die Option **Handzettel** aus:

![](img/6_1_handouts_2.png)

Wir sehen auch gleich in der Vorschau, dass jetzt mehrere Folien gemeinsam auf einer Seite zusammengruppiert werden.

Wenn wir jetzt auf **In Datei drucken...** klicken, können wir einen Speicherort angeben, an den Impress unsere Handouts als PDF-Datei abspeichern soll.

Wenn wir diese PDF-Datei dann öffnen, können wir unsere Handouts bewundern:

![](img/6_1_handouts_3.png)

### Microsoft PowerPoint

Um eine Präsentation für die Verwendung in Microsoft PowerPoint zu exportieren, klicken wir ebenfalls in der Menüleiste zuerst auf **Datei**, dann aber auf **Speichern unter...**.

Wir bekommen wieder ein Fenster angezeigt, in dem wir uns den gewünschten Speicherort für die Datei aussuchen können:

![](img/6_2_export_1.png)

In diesem Fenster können wir jetzt ganz rechts unten das gewünschte Dateiformat auswählen.
In unserem Fall wäre das **Microsoft PowerPoint 2007-2013 XML (.pptx)**.

Wenn wir die Präsentation so exportieren, erhalten wir eine Datei mit der Endung `.pptx`, die wir später in Microsoft PowerPoint öffnen können.
