---
geometry: margin=2cm
---


# Formen, Animationen und Folienübergänge

## Formen

Wir sehen in unserer Tabelle also ganz deutlich, dass LibreOffice Impress das beste Programm zum Erstellen von Präsentationen ist.
Das wollen wir jetzt noch untermauern, indem wir den 1. Rang in der Tabelle mit einem goldenen Stern versehen.

Dazu klicken wir oben in der unteren Symbolleiste auf den nach unten zeigenden Pfeil vom blauen Stern und wählen dann den **Stern mit 5 Zacken**:

![](img/2_8_elements_1.png)

Den Stern zeichnen wir jetzt auf die Folie, indem wir mit der Maus klicken, gedrückt halten, nach rechts unten ziehen und erst dann loslassen, wenn der Stern so groß ist wie wir ihn haben wollen.

Rechts in den Eigenschaften können wir im Punkt **Fläche** die Farbe des Sterns ändern:

![](img/2_8_elements_2.png)

Weiter unten, unter **Linie**, ändern wir die **Linienfarbe** des Sterns auf Schwarz:

![](img/2_8_elements_3.png)

Und außerdem ändern wir hier auch die Breite der Randlinie des Sterns auf **1,5pt**:

![](img/2_8_elements_4.png)

Wenn wir den Stern jetzt noch etwas zurecht rücken, sollte unsere Folie in etwa so aussehen:

![](img/2_8_elements_5.png)

## Formen animieren

Damit wirklich niemand übersehen kann, dass LibreOffice Impress das beste Präsentationserstellungsprogramm ist, wollen wir den Stern jetzt so animieren, dass er in die Folie einfliegt.

Dazu klicken wir ganz rechts auf den Stern (oder klicken alternativ ganz oben in der Menüleiste zuerst auf **Ansicht** und dann auf **Animation**), um im rechten Bereich das Menü für die Animationen zu bekommen:

![](img/2_9_animation_1.png)

Um jetzt unseren Stern zu animieren, markieren wir ihn zuerst und klicken dann im Animationsmenü auf das Plus-Symbol.

Dadurch bekommen wir auch gleich eine Animation, aber nur die Standardmäßige **Erscheinen**-Animation. Da uns die zu langweilig ist und wir unseren Stern ja einfliegen lassen wollen, scrollen wir in der Effekt-Liste bis zur Überschrift **Aufregend** und wählen dort **Spirale** aus:

![](img/2_9_animation_2.png)

Ganz unten gibt es einen Button mit der Aufschrift **Start**. Wenn wir den klicken, können wir uns anschauen, wie unsere Animation aussehen wird.

## Folienübergänge

Wenn wir schon bei Animationen sind, können wir uns auch gleich den Folienübergängen widmen. Die kann man nutzen, um beim Präsentieren beim Umschalten von einer auf die nächste Folie nicht nur ein einfaches, schnelles Aus- und Einblenden zu bekommen, sondern einen etwas dynamischeren Übergang.

Das Menü dafür versteckt sich, genau wie vorher das Menü für die Animationen, ganz rechts, nur diesmal einen Button über dem Stern-Button für die Animationen. Alternativ können wir das Menü auch über die Menüleiste per Klick auf **Ansicht** und **Folienübergang** erreichen:

![](img/2_10_transition_1.png)

Wenn wir hier den Übergang **Rad** auswählen, erhalten wir auch gleich eine Vorschau, wie der Folienübergang aussehen wird.
