
# Building PDFs

``` bash
ls -1 *.md | while read filename; do name="$(echo "$filename" | cut -d "." -f 1)"; pandoc "${name}.md" -o "${name}.pdf"; done
```
