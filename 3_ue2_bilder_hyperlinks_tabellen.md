
# Übungsblatt 2: Bilder, Hyperlinks und Tabellen

1. Erstelle eine neue Präsentation, die aus einer Folie besteht.
2. Suche im Internet nach einem Bild deiner Lieblingsspeise.
3. Füge das Bild in die erste Folie ein.
4. Füge unter das Bild einen Hyperlink ein, der auf die Website führt, von der du das Bild hast.
5. Füge eine zweite Folie ein.
6. Füge in die zweite Folie eine Tabelle ein, die aus 2 Spalten und 3 Zeilen besteht.
7. Schreibe in die Zellen der linken Spalte untereinander: **Name**, **Warm/Kalt**, **Süß/Salzig**.
8. Fülle die rechte Spalte für deine Lieblingsspeise aus.
