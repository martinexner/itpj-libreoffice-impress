
# Übungsblatt 1: Schrift und Layout

1. Erstelle eine neue Präsentation, die aus einer Folie besteht.
2. Schreibe deinen eigenen Namen in **Schriftgröße 20** und in **dunkelgrün** links unten in die Folie.
3. Füge rechts unten eine Aufzählung von 3 Speisen, die du gerne isst, in die Folie ein. Die Aufzählung soll aus **viereckigen Aufzählungspunkten** bestehen.
