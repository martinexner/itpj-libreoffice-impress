---
geometry: margin=2cm
---

# Bilder, Hyperlinks und Tabellen

## Bilder einfügen

Weil Text alleine langweilig ist, wollen wir unsere Folien jetzt mit Bildern versehen. Dazu legen wir wieder eine neue Folie an, wählen diesmal das Layout **Nur Titel** und geben "**Eine Folie mit Bild**" als Titel ein. Die Folie sollte dann so aussehen:

![](img/2_6_image_1.png)

Dann suchen wir uns ein schönes Bild, indem wir einen Webbrowser öffnen, auf Google nach der **TU Wien** suchen, das Suchergebnis mit dem Wikipedia-Eintrag anklicken und dann das Bild vom Hauptgebäude der TU Wien am Karlsplatz anklicken, um es groß anzuzeigen:

![](img/2_6_image_2.png)

Jetzt klicken wir einfach mit der rechten Maustaste auf das Bild und klicken dann auf **Bild kopieren**.

Dann wechseln wir wieder zu LibreOffice Impress (Browserfenster offen lassen, wir werden es gleich nochmal brauchen), machen einen Rechtsklick auf den noch leeren Bereich unserer Folie und wählen **Einfügen**. Und schon haben wir unser Bild auf der Folie:

![](img/2_6_image_3.png)

Allerdings ist das Bild jetzt noch zu groß und verdeckt unseren Titel. Wir werden es daher verkleinern, indem wir an den grünen Ecken klicken, gedrückt halten und mit der Maus die Größe so ändern, dass es den Titel nicht mehr verdeckt:

![](img/2_6_image_4.png)

Anschließend werden wir es noch so verschieben, dass es ein kleines Stück links von der Mitte ist. Dazu mit der Maus **auf** das Bild klicken (nicht wie vorhin auf den Rand), gedrückt halten und ziehen:

![](img/2_6_image_5.png)

Die Folie sollte jetzt so aussehen:

![](img/2_6_image_6.png)

## Hyperlinks einfügen

Wie sich das gehört, werden wir zu unserem Bild jetzt auch noch die Quelle verlinken. Dazu wechseln wir wieder zum Webbrowser und klicken unten rechts auf "**Du musst den Urheber angeben - Zeig mir wie**". Dadurch wird ein Textfeld mit einer Quellenangabe und einer URL angezeigt. Wir kopieren den Inhalt dieses Textfelds in die Zwischenablage, indem wir auf den Kopieren-Button rechts davon klicken:

![](img/2_6_image_7.png)

Dann wechseln wir wieder zurück zum Fenster von LibreOffice Impress und klicken in der oberen Symbolleiste auf den Button mit der Kette (Zwei Buttons rechts neben dem Button für Textfelder), um einen **Hyperlink** einzufügen. Alternativ können wir auch ganz oben in der Menüleiste zuerst auf **Einfügen** und dann auf **Hyperlink** klicken:

![](img/2_6_image_8.png)

Dadurch öffnet sich ein kleines Fenster, in dem wir eingeben können, wohin der Hyperlink führen soll. Hier wählen wir zuerst das Textfeld **Text** (das zweite von unten) und fügen die Quellenangabe ein (mittels Rechtsklick und **Einfügen**, oder *Strg + V* auf der Tastatur), die wir vorher von Wikipedia kopiert haben:

![](img/2_6_image_9.png)

Dann markieren wir von dem Text den wir gerade eben eingefügt haben nur den URL-Teil, also das hinterste Stück, das mit `https://` beginnt und kopieren es (Rechtsklick und **kopieren**, oder *Strg + C* auf der Tastatur). Diese URL fügen wir jetzt im obersten Textfeld ein:

![](img/2_6_image_10.png)

Wenn wir beides erledigt haben, klicken wir auf **OK** und bekommen den Hyperlink in unsere Folie eingefügt:

![](img/2_6_image_11.png)

Jetzt ändern wir zuerst die Schriftgröße auf **12** (rechts in den Eigenschaften, unter "**Zeichen**") und verkleinern und verschieben das Textfeld mit dem Hyperlink so, dass die Folie in etwa so aussieht:

![](img/2_6_image_12.png)

Dann klicken wir auf einen der vier blauen Eckpunkte (halten diesmal aber nicht gedrückt, sondern lassen gleich wieder los). Die Eckpunkte sollten jetzt rot und rund werden:

![](img/2_6_image_13.png)

Jetzt klicken wir auf einen dieser roten, runden Eckpunkte, **halten aber weiter gedrückt**. Dadurch können wir das Textfeld um 90° gegen den Uhrzeigersinn **drehen**, also so, dass man den Text von unten nach oben liest. Die Folie sollte dann so aussehen:

![](img/2_6_image_14.png)

Dann verschieben wir das Hyperlink-Textfeld rechts neben unser Bild, klicken einen der roten, runden Eckpunkte wieder kurz an, damit die Eckpunkte wieder blau werden und wir die größe des Textfelds so ändern können, dass die Folie am Ende so aussieht:

![](img/2_6_image_15.png)

Fertig! Jetzt können wir unsere Präsentationen auch mit Bildern und Hyperlinks versehen!

## Tabellen

Tabellen sind eine wichtige und übersichtliche Darstellungsmöglichkeit für Daten, daher wollen natürlich auch wir sie in unseren Präsentationen verwenden. Dazu erstellen wir wieder eine neue, noch leere Folie, die so aussieht:

![](img/2_7_table_1.png)

Dann klicken wir in der oberen Symbolleiste auf das Tabellen-Symbol, fahren mit dem Mauszeiger so, dass wir 2 Spalten und 4 Zeilen auswählen und im angezeigten, schwebenden Text rechts daneben **2 x 4** steht, und klicken an dieser Stelle dann:

![](img/2_7_table_2.png)

Alternativ könnten wir auch in der Menüleiste zuerst auf **Einfügen** und dann auf **Tabelle** klicken.

Auf unserer Folie befindet sich jetzt eine Tabelle mit 2 Spalten und 4 Zeilen:

![](img/2_7_table_3.png)

Weil uns die Farben dieser Tabelle noch nicht so richtig gefallen, wechseln wir zu einer Tabellenvorlage, in der die oberste Zeile schwarz hinterlegt ist: Dazu markieren wir unsere Tabelle (bzw. lassen sie markiert) und klicken ganz rechts in den Eigenschaften im Punkt **Tabellenvorlagen** auf die zweite Vorlage.

Unsere Folie sieht jetzt so aus:

![](img/2_7_table_4.png)

Jetzt wollen wir die Überschriften unserer Tabelle, also die erste Zeile, befüllen. Dazu klicken wir zuerst doppelt in die linke, oberste Zelle unserer Tabelle und tippen hier den Text "**Rang**" ein. Dann klicken wir doppelt in die rechte, oberste Zelle und tippen dort den Text "**Programm**" ein. Spätestens jetzt fällt uns auf, dass man unseren Text nicht lesen kann. Das liegt daran, dass die Textfarbe die gleiche ist wie die Hintergrundfarbe, nämlich schwarz. Das müssen wir also ändern:

Wir markieren die oberste Zeile der Tabelle, indem wir zuerst auf die linke, oberste Zelle klicken, gedrückt halten, mit dem Mauszeiger bis zur Zelle rechts davon hinüberfahren und erst hier wieder loslassen. Jetzt ist die oberste Zeile (also die beiden obersten Zellen) markiert.

Dann klicken wir ganz rechts in den Eigenschaften unter **Zeichen** wieder auf das bunte, kleine **a** und wählen in der Box ganz rechts oben Weiß aus:

![](img/2_7_table_5.png)

Jetzt können wir unseren eingegebenen Text auch lesen:

![](img/2_7_table_6.png)

Damit das ganze optisch noch ein bisschen ansprechender aussieht, ändern wir unseren Text noch auf fettgedruckt, indem wir ganz rechts in den **Eigenschaften** unter **Zeichen** auf das kleine, graue **a** klicken (Achtung: Nicht verwechseln mit dem **a** für die Schriftfarbe):

![](img/2_7_table_7.png)

Außerdem wollen wir die jeweilige Überschrift mittig in ihrer Zelle stehen haben. Dazu ändern wir zuerst die horizontale Ausrichtung auf zentriert, indem wir, ebenfalls in den **Eigenschaften**, aber diesmal unter **Absatz**, den entsprechenden Button klicken:

![](img/2_7_table_8.png)

Und dann ändern wir noch die vertikale Ausrichtung auf mittig, indem wir den entsprechenden Button rechts davon klicken:

![](img/2_7_table_9.png)

Zum Schluss setzen wir noch die Schriftgröße auf **22**:

![](img/2_7_table_10.png)

Dann fehlt uns eigentlich nur noch der Inhalt der Tabelle: Hierfür tippen wir, von oben nach unten:

- In die zweite Zeile zuerst "**1**" in die linke Zelle und dann "**LibreOffice Impress**" in die rechte Zelle
- In die dritte Zeile "**2**" in die linke und "**Google Slides**" in die rechte Zelle
- In die vierte und letzte Zeile "**3**" und "**Microsoft PowerPoint**"

Dann setzen wir noch für diese 3 Zeilen die **vertikale** Ausrichtung auf mittig, so wie wir es vorhin für die oberste Zeile (unsere Überschriften) gemacht haben. Von der linken Spalte setzen wir zusätzlich die **horiztontale** Ausrichtung auf zentriert.

Unsere Folie sollte jetzt so aussehen:

![](img/2_7_table_11.png)

Und damit hätten wir auch das Thema Tabellen gemeistert!
