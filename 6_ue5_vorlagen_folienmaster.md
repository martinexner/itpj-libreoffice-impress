
# Übungsblatt 5: Vorlagen, Folienmaster

1. Erstelle eine neue Präsentation, die aus vier Folien besteht.
2. Gib den ersten beiden Folien einen Titel deiner Wahl.
3. Stelle für die anderen beiden Folien jeweils andere Vorlagen ein.
4. Ändere die Schriftfarbe des Titels der ersten beiden Folien, indem du ihre Masterfolie entsprechend anpasst. Ändere die Schriftfarbe nicht auf den Folien direkt!
