---
geometry: margin=2cm
---

# Allgemein

LibreOffice Impress ist ein Programm zur Erstellung von Präsentationen, das auf den Betriebssystemen Linux, MacOS und Windows verfügbar ist.

Präsentationsprogramme wie LibreOffice Impress werden vor allem zur Unterstützung von Vorträgen in Firmen, in Schulen, an Universitäten und bei Konferenzen eingesetzt.


# Erste Schritte

## LibreOffice Impress starten

Wenn wir LibreOffice Impress starten, werden wir mit einem Dialog begrüßt, der uns eine Vorlage wählen lässt:

![](img/1_1_new_file_dialog.png)

Hier klicken wir auf **Abbrechen**, weil wir fürs Erste keine Vorlage verwenden werden. Wir werden aber später nochmal auf Vorlagen zurückkommen.

Jetzt sehen wir die normale Ansicht von Impress mit unserer bisher einzigen, leeren Folie:

![](img/1_2_empty_presentation.png)

Hier gibt es 3 wichtige Bereiche:

1. Ganz links sehen wir die **Liste der Folien**, im Bild mit einer rot unterlegten **1** markiert. In dieser Liste gibt es im Moment nur eine leere Folie.
2. Im großen Bereich in der Mitte wird die **aktuell ausgewählte Folie** dargestellt. Das ist im Bild mit der rot unterlegten **2** markiert.
3. Ganz rechts (im Bild der mit der rot unterlegten **3** markierte Bereich) befinden sich die **Eigenschaften**. Hier gibt es diverse Einstellungen zur aktuell ausgewählten Folie. Hier werden wir in unseren nächsten Schritten den Hauptteil der Einstellungen vornehmen.

## Präsentation speichern

Es ist immer eine gute Idee, die Präsentation gleich zu Beginn abzuspeichern. LibreOffice speichert dann nämlich automatisch regelmäßig zwischen und verhindert so, dass wir unsere Arbeit verlieren, falls das Programm oder der Computer abstürzen.

Dazu klicken wir ganz links oben auf **Datei** und wählen aus dem Menü **Speichern unter...** aus. Hier suchen wir einen Speicherort aus (z.B. **Dokumente**), vergeben einen Namen (z.B. **`Praesentation_1`**) und klicken auf **Speichern**.

## Präsentation starten

Die Präsentation starten, so wie wir das bei einem Vortrag dann auch machen würden, geht auf mehrere Arten:

- Indem wir in der oberen Symbolleiste auf den Button mit den Bildschirm- und Play-Symbolen klicken, oder
- Indem wir ganz oben in der Menüleiste zuerst auf **Bildschirmpräsentation** und dann, je nachdem was wir wollen, auf **Von der ersten Folie** oder auf **Von der aktuellen Folie** klicken, oder
- Indem wir auf der Tastatur die **F5**-Taste drücken.

Die Präsentation startet dann im Vollbildmodus.
Zur nächsten Folie kommt man immer mit der **Leertaste** oder mit der **Pfeiltaste nach rechts**, zur vorigen Folie mit der **Pfeiltaste nach links**.
Beenden lässt sich die Präsentation mit der **Escape**-Taste.

Aktuell ist das ganze noch relativ unspektakulär, weil wir in unserer Präsentation erst eine Folie haben und diese außerdem leer ist.
Aber in den kommenden Abschnitten werden wir nach und nach Folien hinzufügen, es lohnt sich also, immer wieder eine Bildschirmpräsentation zu starten, um zu sehen, wie das Ganze langsam Form annimmt!
