---
geometry: margin=2cm
---


# Vorlagen und Folienmaster

## Vorlagen

Ganz am Anfang, als wir LibreOffice Impress zum ersten mal geöffnet haben und unsere neue Präsentation erstellt haben, haben wir schon einmal Vorlagen zur Auswahl bekommen, uns aber dafür entschieden unsere Folien komplett selbst zu gestalten.

Jetzt wollen wir uns diese Vorlagen nochmal anschauen, aber nicht für die ganze Präsentation, sondern für einzelne Folien.

Dazu erstellen wir wieder eine neue Folie:

![](img/3_1_templates_1.png)

Um zu den Vorlagen zu gelangen, klicken wir ganz rechts auf den **Folienmaster**-Button (der blaue Button unter dem Animations-Button mit dem Stern-Symbol).
Was Folienmaster sind, werden wir uns gleich auch noch anschauen, aber im Moment ist es noch nicht weiter wichtig.

Hier wählen wir aus der unteren Liste ("Zur Verwendung vorhanden") eine Vorlage, die uns gefällt.
Die Folie übernimmt dann automatisch unter anderem die Schriftarten, Schriftfarben, Schriftgrößen, Positionen der Textfelder und die Hintergrundfarbe bzw. das Hintergrundbild der Vorlage.

Wenn wir der Folie jetzt noch einen passenden Titel geben, sollte sie ca. so aussehen:

![](img/3_1_templates_2.png)


## Folienmaster

Wie im vorigen Abschnitt schon angekündigt, werden wir uns hier auch kurz ansehen, was **Folienmaster** sind.

Ein Folienmaster ist im Wesentlichen selber eine Folie, die allerdings in der Präsentation nicht angezeigt wird, sondern nur dazu da ist, eine Vorlage für die normalen Folien zu sein.
Jede normale Folie (also solche, die bei der Präsentation angezeigt werden) hat eine Masterfolie, die ihre Vorlage ist.
Deshalb war im vorigen Abschnitt die Auswahl der Vorlagen für die Folien auch im Bereich **Folienmaster**.

Normale Folien übernehmen Dinge wie Schriftgrößen, -arten, -farben und Hintergrundbilder oder -farben standardmäßig von ihrer Masterfolie, außer man ändert es für die jeweilige Folie explizit.

Das heißt, wenn ich eine Masterfolie bearbeite, indem ich zum Beispiel ihre Hintergrundfarbe ändere, dann ändert sich auch die Hintergrundfarbe aller normalen Folien mit, die diese Masterfolie als ihre Masterfolie gesetzt haben und die wird nicht selber schon vorher mit einer anderen Hintergrundfarbe versehen haben.

Das erleichtert es einem wesentlich, alle Folien in einer Präsentation einheitlich zu halten, weil man beispielsweise die Hintergrundfarbe nicht für jede Folie einzeln einstellen muss, sondern nur ein einziges mal (nämlich in der Masterfolie) und sie sich dadurch in allen Folien anpasst.

Und das werden wir auch gleich testen!

Dazu wählen wir wieder unsere allererste Folie aus und klicken ganz rechts auf den Eigenschaften-Button (im Screenshot hat er ein Schraubenschlüssel-Symbol) um zu den Eigenschaften zu kommen (falls die Eigenschaften nicht sowieso schon angezeigt werden):

![](img/4_1_master_1.png)

Hier klicken wir jetzt auf den **Masteransicht**-Button (alternativ können wir auch wieder ganz oben in der Menüleiste zuerst auf **Ansicht** und dann auf **Folienmaster** klicken).
Dadurch kommen wir zu der Ansicht, in der sich die Masterfolien bearbeiten lassen:

![](img/4_1_master_2.png)

Hier sehen wir jetzt, dass es in unserer Präsentation zwei solche Masterfolien gibt.
Das liegt daran, dass wir im vorigen Abschnitt für unsere letzte Folie eine neue Vorlage ausgewählt haben.
Diese Vorlage war nichts anderes als eine andere Masterfolie.
Diese letzte Folie verwendet also die zweite Masterfolie aus der Liste die wir hier ganz links sehen (die mit dem bunten Hintergrund) als ihre Vorlage und alle anderen Folien unserer Präsentation verwendet die erste Masterfolie als ihre Vorlage.

Aber zurück zum eigentlichen Thema: Wir wollen die Hintergrundfarbe unserer ersten Masterfolie ändern. Dazu wählen wir ganz links die erste Masterfolie aus (nur zur Sicherheit, falls wir inzwischen die zweite Masterfolie angeklickt haben).
Ganz rechts sind wieder die Eigenschaften, wie bei normalen Folien auch.
Hier gibt es relativ weit oben den Punkt **Hintergrund**.
Aus dem Dropdown wählen wir **Farbe** und stellen dann z.B. **Hellgold 4** als Farbe ein.
Jetzt hat unsere erste Masterfolie einen anderen Hintergrund:

![](img/4_1_master_3.png)

Wenn wir die Masteransicht jetzt wieder schließen, indem wir entweder ganz oben in der oberen Symbolleiste ganz rechts, oder ganz rechts am unteren Ende der Eigenschaften auf **Masteransicht schließen** klicken, gelangen wir wieder zurück zu der Ansicht, in der wir unsere normalen Folien bearbeiten können.

Und siehe da: Alle bis auf die letzte Folie haben jetzt den Hintergrund übernommen, sehr praktisch!

![](img/4_1_master_4.png)
