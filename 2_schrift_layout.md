---
geometry: margin=2cm
---

# Schrift und Layout

## Titelfolie erstellen

Als nächstes wollen wir unserer bisher leeren Präsentation etwas Inhalt verleihen. Dazu werden wir zuerst unsere erste und einzige Folie zu unserer Titelfolie machen. Das machen wir, indem wir in den **Eigenschaften** (zur Erinnerung: Der Bereich ganz rechts) unter **Layouts** das Layout "**Zentrierter Text**" auswählen.

Unsere Folie sollte dann so aussehen:

![](img/2_1_title_slide_0.png)

Wenn wir jetzt auf "**Text durch Klicken hinzufügen**" klicken, können wir unseren eigenen Titeltext eingeben. Hier schreiben wir zum Beispiel: "**Ich bin ein zentrierter Titel**". Unsere Folie sieht dann so aus:

![](img/2_1_title_slide_1.png)

## Schriftgröße, -art und -farbe anpassen

Um unsere Folie optisch etwas ansprechender zu machen, werden wir die Schriftgröße, die Schriftart und die Schriftfarbe des Titels anpassen.

Zuerst vergrößern wir die Schriftgröße auf **44**, indem wir

1. den Text, den wir anpassen möchten, also den Text "**Ich bin ein zentrierter Text**", mit der Maus markieren und anschließend
2. in den Eigenschaften unter **Zeichen** im rechten Dropdown die **44** auswählen:

![](img/2_2_title_slide_2.png)

Dann ändern wir die Schriftart, indem wir

1. wieder den Text, den wir anpassen möchten, markieren und anschließend
2. in den Eigenschaften unter **Zeichen** im linken Dropdown die Schriftart **Liberation Serif** auswählen:

![](img/2_2_title_slide_3.png)

Außerdem passen wir die Schriftfarbe an, indem wir

1. wieder den Text, den wir anpassen möchten, markieren und anschließend
2. in den Eigenschaften unter **Zeichen** das kleine, rot unterstrichene **a** anklicken und in der Farbpalette **Dunkelblau** auswählen:

![](img/2_2_title_slide_4.png)

Zum Schluss verschieben wir unseren Titeltext noch in die Mitte, indem wir

1. den hell-orangen, äußeren Rand um unseren Titeltext (also um "Ich bin ein zentrierter Text") anklicken und anschließend
2. durch klicken und gedrückt halten mit der Maus ein Stück nach unten ziehen.

Das ganze sollte während dem Ziehen in etwa so aussehen:

![](img/2_2_title_slide_5.png)

Am Ende sollte unsere Titelfolie so aussehen:

![](img/2_2_title_slide_final.png)

## Eine Folie hinzufügen (und Folien verschieben und wieder löschen), Aufzählungen

Zeit für eine neue Folie! Um eine anzulegen, klicken wir mit der **rechten** Maustaste links in der Folienliste auf den leeren, weißen Bereich und wählen **Neue Folie** aus. Wir bekommen eine neue Folie:

![](img/2_3_new_slide.png)

Falls wir einmal eine Folie zu viel anlegen, können wir sie auf fast die gleiche Art auch wieder löschen: Einfach links in der Liste der Folien mit der **rechten** Maustaste auf die Folie klicken, die gelöscht werden soll, und **Folie löschen** auswählen.

Und falls wir die Reihenfolge unserer Folien ändern möchten, können wir das ganz einfach bewerkstelligen, indem wir links in der Liste der Folien auf einzelne Folien mit der **linken** Maustaste klicken **und gedrückt halten** und die Folie dann einfach an die gewünschte Stelle ziehen.

In dieser Folie wollen wir eine Aufzählung hinzufügen. Dazu ändern wir zuerst das Layout der Folie auf etwas passendes, in dem wir rechts in den **Eigenschaften** das Layout "**Titel, Inhalt**" auswählen. Die Folie sollte dann so aussehen:

![](img/2_4_enumeration_1.png)

Wir vergeben zuerst einen passenden Titel, indem wir in der oberen Textbox auf "**Titel durch Klicken hinzufügen**" hinzufügen und "**Eine Aufzählung**" eintippen.

Wenn wir jetzt auf "**Text durch Klicken hinzufügen**" klicken, können wir Aufzählunkspunkte hinzufügen, indem wir Text eingeben und anschließend, um einen weiteren Aufzählungspunkt hinzuzufügen, auf der Tastatur **Enter** drücken und wieder Text eingeben.

Wir

1. geben zuerst "**Erstens**" ein,
2. Drücken **Enter** und geben "**Zweitens**" ein,
3. Drücken wieder **Enter** und geben "**Drittens**" ein,
4. Drücken noch ein letztes mal **Enter** und geben abschließend "**...**" ein.

Unsere Folie sollte dann so aussehen:

![](img/2_4_enumeration_2.png)

Das wäre dann eine Aufzählung mit Bullet-Points.

Angenommen wir wollen aber eine nummerierte Aufzählung. Dann müssen wir die Aufzählung auf nummeriert umschalten, indem wir

1. Den Text, den wir anpassen wollen, wieder markieren und
2. Rechts in den **Eigenschaften** unter **Absatz** den Button "**Nummerierte Liste umschalten**" (der 2. von Links in der 2. Zeile unter "Absatz") klicken. **Achtung**: Nicht auf den kleinen nach Unten zeigenden Pfeil rechts vom Button klicken, sondern auf den Button selber. (Zu diesem Pfeil kommen wir aber gleich noch)

![](img/2_4_enumeration_3.png)

Unsere Folie sollte jetzt so aussehen:

![](img/2_4_enumeration_4.png)

Weil uns das noch nicht so richtig gefällt, wählen wir eine andere Nummerierungsart, indem wir

1. Wieder unseren Text markieren (bzw. er immernoch markiert ist) und
2. Wir diesmal nicht auf den kleinen "**Nummerierte Liste umschalten**"-Button direkt, sondern auf den noch kleineren, nach Unten zeigenden Pfeil gleich rechts daneben klicken:

![](img/2_4_enumeration_5.png)

Hier wählen wir die 2. Option von Links in der 1. Reihe ("**Nummer 1. 2. 3.**"). Unsere Aufzählung sollte jetzt so aussehen:

![](img/2_4_enumeration_6.png)

Fast perfekt! Aber leider nur fast. Die Nummern kleben links an unserem Text, das möchten wir ändern, ohne mühsam in jeder Zeile selbst einen Abstand eintippen zu müssen. Dazu

1. Markieren wir wieder unseren Text,
2. Klicken nochmal auf den kleinen nach Unten zeigenden Pfeil rechts neben dem "**Nummerierte Liste umschalten**"-Button und
3. Klicken diesmal ganz unten auf "**Weitere Nummerierungen**":

![](img/2_4_enumeration_7.png)

Jetzt öffnet sich ein kleines Dialogfenster mit dem Titel "**Aufzählungszeichen und Nummerierung**". Hier müssen wir im Tab "**Benutzerdefiniert**" das unterste Textfeld ("**Hinter**") anpassen, indem wir hinter den Punkt, der schon dort steht, einen Abstand eintippen. Der Inhalt des Textfeldes wird also von "**`.`**" zu "**`. `**":

![](img/2_4_enumeration_8.png)

Wenn wir jetzt auf **OK** klicken, sollte unsere Folie so aussehen:

![](img/2_4_enumeration_9.png)

Jetzt ist unsere Aufzählung perfekt!

## Folien mit eigenem Layout

Nachdem wir jetzt schon einige vorgegebenen Folien-Layouts kennengelernt haben, wollen wir jetzt eine Folie mit einem ganz eigenen Layout erstellen. Dazu legen wir zuerst wieder eine neue Folie an (Links in der Liste der Folien Rechtsklick und dann auf **Neue Folie**). Für diese Folie wählen wir dann in den Eigenschaften ganz rechts das Layout "Leere Folie" aus (das erste Layout in der Liste).

Die Folie sollte dann so aussehen:

![](img/2_5_own_layout_1.png)

Jetzt fügen wir ein **Textfeld** in die Folie ein, indem wir oben in der oberen der beiden Symbolleisten den Button mit dem getrichelt umrandeten **T** anklicken (Alternativ können wir auch ganz oben in der Menüleiste zuerst auf **Einfügen** und dann auf **Textfeld** klicken).

Dadurch ändert sich der Mauszeiger zu einem Kreuz und wir können an einer Stelle in der Folie klicken, gedrückt halten und das Textfeld dort aufspannen, wo wir es haben wollen:

![](img/2_5_own_layout_2.png)

Wenn wir den Mauszeiger loslassen, erhalten wir im ausgewählten Bereich unser Textfeld und können Text hineintippen, zum Beispiel den Text "**Eine Folie mit eigenem Layout**". Unsere Folie sollte dann so aussehen:

![](img/2_5_own_layout_3.png)

Und schon haben wir eine Folie mit eigenem Layout! Wir sehen also: Wir können die vorgegebenen Layouts verwenden, müssen aber nicht.
